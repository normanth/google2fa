<?php

/**
 * PHP Class for handling Encryption an decryption in database
 *
 * @class Google2FACrypt
 * @author Norman Thimm, Mark Redel
 * @copyright 2015 Norman Thimm, 2020 Mark Redel
 * @license http://www.gnu.org/licenses/ GNU Affero General Public License
 * @link http://www.familiethimm.de/
 */
class Google2FACrypt {

	private static $encr = null;
	private static $td = null;
	private static $iv = null;
	private static $key = null;
	private static $method = null;

	/**
	 * Initialization type of cryption
	 *
	 * @return string
	 */
	protected static function initCryption() {
		if (self::$encr === null) {
			self::$encr = Google2FAData::getCryption(); // cryption of user
			if (self::$encr === "") { // no cryption set -> select cryption for new 2FA user
				if (extension_loaded("mcrypt") && function_exists("mcrypt_encrypt") && (PLUGIN_GOOGLE2FA_CRYPT == 'mcrypt' || PLUGIN_GOOGLE2FA_CRYPT == 'auto'))
                                        self::$encr = "mcrypt";
				else if (extension_loaded("openssl") && function_exists("openssl_encrypt") && (PLUGIN_GOOGLE2FA_CRYPT == 'openssl' || PLUGIN_GOOGLE2FA_CRYPT == 'auto'))
                                        self::$encr = "openssl";
				else if (function_exists("base64_encode") && (PLUGIN_GOOGLE2FA_CRYPT == 'base64' || PLUGIN_GOOGLE2FA_CRYPT == 'auto'))
					self::$encr = "base64";
				else if (PLUGIN_GOOGLE2FA_CRYPT == 'no')
					self::$encr = "no";
				else throw new Exception('Cannot load requested cryption mode');
				Google2FAData::setCryption(self::$encr);
			}
			if (self::$encr === "mcrypt") { // user cryption is mcrypt
				if (!extension_loaded("mcrypt") || !function_exists("mcrypt_encrypt"))
					throw new Exception('Cannot load cryption mode mcrypt');
				self::$td = mcrypt_module_open(PLUGIN_GOOGLE2FA_MCRYPTALGORITHM, '', PLUGIN_GOOGLE2FA_MCRYPTMODE, '');
				self::$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size(self::$td), PLUGIN_GOOGLE2FA_MCRYPTRAND);
				$ks = mcrypt_enc_get_key_size(self::$td);
				if (!defined('PLUGIN_GOOGLE2FA_MCRYPTKEY') || strlen(PLUGIN_GOOGLE2FA_MCRYPTKEY) < $ks)
	                                throw new Exception('MCRYPT key not set correctly (needs '.$ks.' characters)');
				else
					self::$key = substr(md5(PLUGIN_GOOGLE2FA_MCRYPTKEY), 0, $ks);
			}
			else if (self::$encr === "openssl") { // user cryption is openssl
				if (!extension_loaded("openssl") || !function_exists("openssl_encrypt"))
					throw new Exception('Cannot load cryption mode openssl');
				if (!defined('PLUGIN_GOOGLE2FA_OPENSSL_KEY') || strlen(PLUGIN_GOOGLE2FA_OPENSSL_KEY) < 16)
					throw new Exception('OpenSSL key not set correctly (min 16 characters)');
				else
					self::$key = PLUGIN_GOOGLE2FA_OPENSSL_KEY;
				if (!defined('PLUGIN_GOOGLE2FA_OPENSSL_CIPHER'))
					throw new Exception('OpenSSL cipher not set');
				else if (!in_array(PLUGIN_GOOGLE2FA_OPENSSL_CIPHER, openssl_get_cipher_methods(true)))
					throw new Exception('OpenSSL cipher not available - see openssl_get_cipher_methods()');
				else
					self::$method = PLUGIN_GOOGLE2FA_OPENSSL_CIPHER;
				if (version_compare(PHP_VERSION, '5.3.3', '<'))
					throw new Exception('OpenSSL requires at least PHP version 5.3.3');
				else if (version_compare(PHP_VERSION, '7.1.0', '<') &&
						 ((substr_compare(self::$method, 'gcm', -3, 3, true) == 0) || (substr_compare(self::$method, 'ccm', -3, 3, true) == 0)))
					throw new Exception('OpenSSL requires at least PHP version 7.1.0 for cypher method '.self::$method);
			}
		}
	}

        /**
         * Encrypt a string
         *
	 * @param string $a text to encrypt
         * @return string
         */
	public static function encrypt($a) {
		self::initCryption();
		if ($a === "") {
			return $a;
		} else if (self::$encr === "no") {
			return $a;
		} else if (self::$encr === "base64") {
			return base64_encode($a);
		} else if (self::$encr === "mcrypt") {
			mcrypt_generic_init(self::$td, self::$key, self::$iv);
			$a = mcrypt_generic(self::$td, $a);
			mcrypt_generic_deinit(self::$td);
			return trim(base64_encode($a));
		} else if (self::$encr === "openssl") {
			$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::$method));
			$tag = null;
			if (version_compare(PHP_VERSION, '7.1.0', '<'))
				$secret = openssl_encrypt($a, self::$method, self::$key, 0, $iv); // needs PHP 5.3.3 (param iv)
			else
				$secret = openssl_encrypt($a, self::$method, self::$key, 0, $iv, $tag, '', 16); // needs PHP 7.1.0 (param tag, tag_len)
			$a = 'v0:'.$secret.':'.base64_encode($iv).':'.base64_encode($tag);
			return trim($a);
		}
		else throw new Exception('No supported cryption mode loaded');
		return null;
	}

        /**
         * Decrypt a string
         *
         * @param string $a text to decrypt
         * @return string
         */
	public static function decrypt($a) {
                self::initCryption();
                if ($a === "") {
			return $a;
		} else if (self::$encr === "no") {
			return $a;
		} else if (self::$encr === "base64") {
                        return base64_decode($a);
                } else if (self::$encr === "mcrypt") {
			mcrypt_generic_init(self::$td, self::$key, self::$iv);
			$a = mdecrypt_generic(self::$td, base64_decode($a));
			mcrypt_generic_deinit(self::$td);
			return trim($a);
		} else if (self::$encr === "openssl") {
			if (strlen($a) > 0) {
				$a_arr = explode(':', $a);
				if ($a_arr[0] === 'v0') {
					if (version_compare(PHP_VERSION, '7.1.0', '<') && (base64_decode($a_arr[3]) != null))
						throw new Exception('OpenSSL ecrypted secret with tag requires at least PHP version 7.1.0');
					else if (version_compare(PHP_VERSION, '7.1.0', '<') || (base64_decode($a_arr[3]) == null))
						$a = openssl_decrypt($a_arr[1], self::$method, self::$key, 0, base64_decode($a_arr[2]));
					else
						$a = openssl_decrypt($a_arr[1], self::$method, self::$key, 0, base64_decode($a_arr[2]), base64_decode($a_arr[3]));
					return trim($a);
				}
				else throw new Exception('OpenSSL cipher text '.$a_arr[0].' not supported (latest is v0)');
			}
		} else throw new Exception('No supported cryption mode loaded');
	}
}

?>
